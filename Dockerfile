# this is taken from the original gitlab ci/cd dockerfile, and adjusted:
# https://gitlab.com/pages/hugo/-/releases

# Dockerfile for Hugo (HUGO=hugo) / Hugo Extended (HUGO=hugo_extended)
# HUGO_VERSION / HUGO_SHA / HUGO_EXTENDED_SHA is automatically updated
# by update.py when new release is available on the upstream.
# Utilize multi-stage builds to make images optimized in size.

# First stage - download prebuilt hugo binary from the GitHub release.
# Use golang image to run https://github.com/yaegashi/muslstack
# on hugo executable to extend its default thread stack size to 8MB
# to work around segmentation fault issues.
#FROM golang:latest
FROM debian:latest
RUN apt update -y && \
	apt install -y tar git curl jq ca-certificates openssl lftp nodejs && \
	apt clean && \
	rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    curl https://www.npmjs.com/install.sh | sh 
#RUN go get github.com/yaegashi/muslstack
#RUN muslstack -s 0x800000 /usr/bin/hugo

# now we can download the latest build of hugo-extended,
# see script for the magic
COPY get-hugo.sh /home/hugo/get-hugo.sh
RUN /home/hugo/get-hugo.sh	&& \
	rm -f /home/hugo/get-hugo.sh && \
	hugo version

# Second stage - build the final image with minimal apk dependencies.
# alpine:edge is required for muslstack to work as of June 2019.
#FROM debian:latest
#ARG HUGO=hugo
# COPY --from=0 /usr/bin/hugo /usr/bin
# RUN set -eux && \
#     case ${HUGO} in \
#       *_extended) \
#         apk add --update --no-cache libc6-compat libstdc++ && \
#         rm -rf /var/cache/apk/* ;; \
#     esac && \
#     hugo version
EXPOSE 1313
WORKDIR /src
CMD ["/usr/bin/hugo"]
