#!/bin/bash

set -eux

# github
G_OWNER=gohugoio 
G_REPO=hugo
G_URL="https://api.github.com/repos/${G_OWNER}/${G_REPO}/releases"

LATEST=`curl -s ${G_URL}/latest | jq -r ".tag_name"`
echo "latest version is $LATEST"

G_PKG="https://github.com/gohugoio/hugo/releases/download/${LATEST}"
PKG="hugo_extended_${LATEST:1}_Linux-64bit.tar.gz"

curl -L ${G_PKG}/${PKG} -o /home/hugo/${PKG} -s
tar -xvf /home/hugo/$PKG -C /usr/bin/ hugo
rm -f /home/hugo/${PKG}

