# docker-hugo-extended
The reason why i created this repo overcome some issues w/ ftp while deploying hugo generated pages. the hugo docker that i was using for CI/CD is alpine linux based and the lftp in it is built using openssl instead gnutls, which wasn't working with my ftp server.
So this is a temporary workaround to overcome this issue.

For list of images see [gitlab container registry](https://gitlab.com/bugrasan/docker-hugo-extended/container_registry).
```bash
docker run -it --rm registry.gitlab.com/bugrasan/docker-hugo-extended:<tag>
```
